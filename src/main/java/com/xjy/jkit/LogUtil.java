package com.xjy.jkit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;

public abstract class LogUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogUtil.class);

    public static Logger getLogger() {
        return LOGGER;
    }

    public static void debug(String msg) {
        LOGGER.debug("###DEBUG### " + msg);
    }

    public static void info(String msg) {
        LOGGER.info("###INFO### " + msg);
    }

    public static void warn(String msg) {
        LOGGER.warn("###WARN### " + msg);
    }

    public static void error(String msg, Throwable t) {
        LOGGER.error("###ERROR### " + msg, t);
    }

    public static void error(String msg) {
        LOGGER.error("###ERROR### " + msg);
    }
}
