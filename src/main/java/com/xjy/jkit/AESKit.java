package com.xjy.jkit;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

public class AESKit {

    private static final String AESTYPE = "AES/ECB/PKCS5Padding";

    public static String encrypt(String keyStr, String plainText) throws Exception {
        try {
            Key key = generateKey(keyStr);
            Cipher cipher = Cipher.getInstance(AESTYPE);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] encrypt = cipher.doFinal(plainText.getBytes());
            return new String(Base64.encodeBase64(encrypt));
        } catch (Exception e) {
            LogUtil.error("加密异常", e);
            throw e;
        }
    }

    public static String decrypt(String keyStr, String encryptData) throws Exception {
        try {
            Key key = generateKey(keyStr);
            Cipher cipher = Cipher.getInstance(AESTYPE);
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] decrypt = cipher.doFinal(Base64.decodeBase64(encryptData));
            return new String(decrypt).trim();
        } catch (Exception e) {
            LogUtil.error("解密异常", e);
            throw e;
        }
    }

    private static Key generateKey(String key) throws Exception {
        try {
            SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(), "AES");
            return keySpec;
        } catch (Exception e) {
            throw e;
        }
    }

    public static void main(String[] args) {

        String keyStr = "UITN25LMUQC436IM";

        String plainText = "肖建宇肖建宇";
        try {

            String encText = encrypt(keyStr, plainText);
            String decString = decrypt(keyStr, encText);
            System.out.println(encText);
            System.out.println(decString);
        } catch (Exception e) {
            LogUtil.error("", e);
        }


    }
}
