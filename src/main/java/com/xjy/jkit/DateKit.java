package com.xjy.jkit;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public abstract class DateKit {


    /**
     * 获取指定天数后的工作日
     * xiaojianyu
     * @param startDay 开始日期
     * @param days     指定天数，0表示当天，必须是正整数，不能是负数
     * @param holidays [格式必须 yyyy-MM-dd ] 自定义节日list,包含在此的日期会认为是节日，会直接跳过
     * @param customWorkDays [格式必须 yyyy-MM-dd ] 自定义工作日list,包含在此的日期会认为是工作日，
     *                       主要是为了考虑周六日或者法定节假日上班的特殊情况，如果holidays和customWorkDays都存在相同的日期，
     *                       则把这个日期当做工作日计算
     *
     * @return
     * @throws ParseException
     */
    public static Date getWorkDays(Date startDay, int days, List<String> holidays, List<String> customWorkDays) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        String startDayYmd = sdf.format(startDay);
        calendar.setTime(sdf.parse(startDayYmd));

        if (days != 0) {
            int i = 0;
            while (i < days) {
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                if (!isHolidays(sdf.format(calendar.getTime()), holidays, customWorkDays) && !isDoubleCease(calendar, sdf, customWorkDays)) {
                    ++i;
                }
            }
        } else {
            while (isHolidays(sdf.format(calendar.getTime()), holidays, customWorkDays) || isDoubleCease(calendar, sdf, customWorkDays)) {
                calendar.add(Calendar.DAY_OF_MONTH, 1);
            }
        }

        // 把原日期的时分秒设置到新的日期上面
        Calendar calendarSrc = Calendar.getInstance();
        calendarSrc.setTime(startDay);
        calendar.set(Calendar.MINUTE, calendarSrc.get(Calendar.MINUTE));
        calendar.set(Calendar.SECOND, calendarSrc.get(Calendar.SECOND));
        calendar.set(Calendar.HOUR, calendarSrc.get(Calendar.HOUR_OF_DAY));

        return calendar.getTime();
    }


    /**
     * 判断是否是双休日（周六日）,排除自定义工作日
     *
     * @param calendar
     * @param customWorkDays
     * @return
     */
    private static boolean isDoubleCease(Calendar calendar, SimpleDateFormat sdf, List<String> customWorkDays) {
        String dateYmd = sdf.format(calendar.getTime());
        if ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
                || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
                && (!customWorkDays.contains(dateYmd))) {
            return true;
        }
        return false;
    }

    /**
     * 判断是否是节日
     *
     * @param dateStr
     * @param holidays
     * @param customWorkDays
     * @return
     */
    private static boolean isHolidays(String dateStr, List<String> holidays, List<String> customWorkDays) {
        if (holidays.contains(dateStr) && !customWorkDays.contains(dateStr)) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        List<String> h = new ArrayList<String>();
        h.add("2017-12-30");
        h.add("2017-12-31");
        h.add("2018-01-01");

        List<String> workDays = new ArrayList<String>();
        workDays.add("2018-01-01");
        try {
            long start = System.currentTimeMillis();
            System.out.println(DateKit.getWorkDays(new Date(), 3, h, workDays));
            System.out.println(System.currentTimeMillis() - start);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
